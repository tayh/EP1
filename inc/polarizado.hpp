#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP 

#include <iostream>
#include "filtro.hpp"

class Polarizado : public Filtro {
public:

	Polarizado();
	~Polarizado();
	Polarizado(int largura, int altura, int valorMaximo);

	void guardaPixel(ofstream &arquivo2);
};

#endif