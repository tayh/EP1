#ifndef MEDIA_HPP
#define MEDIA_HPP 

#include <iostream>
#include "filtro.hpp"

class Media : public Filtro {
public:

	Media();
	~Media();
	Media(int largura, int altura);

	void guardaPixel(ofstream &arquivo2);
};

#endif