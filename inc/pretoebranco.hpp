#ifndef PRETOEBRANCO_HPP
#define PRETOEBRANCO_HPP 

#include <iostream>
#include "filtro.hpp"

class Pretoebranco : public Filtro {
public:

	Pretoebranco();
	~Pretoebranco();
	Pretoebranco(int largura, int altura);

	void guardaPixel(ofstream &arquivo2);
};

#endif