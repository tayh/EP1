#include <iostream>
#include <cstdlib>
#include <fstream>
#include "imagem.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoebranco.hpp"
#include "media.hpp"


int main() {

	int operacao;
	cout << "\n\n--------------------------------- FILTRO FELIZ ---------------------------------------" << endl;
	cout << "\n\n Filtros disponiveis: " << endl;
	cout << "1. Negativo" << endl;
	cout << "2. Polarizado" << endl;
	cout << "3. Preto e Branco" << endl;
	cout << "4. Media" << endl;
	cout << "0. Sair do Filtro Feliz" << endl;
	cout <<"\n\n----------------------------------------------------------------------------------------" << endl;
	cout << "Escolha uma opção: " << endl;
	cin >> operacao;


	Negativo *negativo = new Negativo();
	Polarizado *polarizado = new Polarizado();
	Pretoebranco *pretoebranco = new Pretoebranco();
	Media *media = new Media();

	ofstream arquivo2;

	switch (operacao) {
		case 0:
			system("clear");
			cout << "Bye Bye" << endl;
			break;
		case 1:
			negativo->lerImagem(arquivo2);
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n" << endl;
			cout << "Imagem filtrada com sucesso, olhe na pasta doc desse programa." << endl;
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			break;
		case 2:
			polarizado->lerImagem(arquivo2);
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n" << endl;
			cout << "Imagem filtrada com sucesso, olhe na pasta doc desse programa." << endl;
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			break;
		case 3:
			pretoebranco->lerImagem(arquivo2);
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n" << endl;
			cout << "Imagem filtrada com sucesso, olhe na pasta doc desse programa." << endl;
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			break;
		case 4:
			media->lerImagem(arquivo2);
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n" << endl;
			cout << "Imagem filtrada com sucesso, olhe na pasta doc desse programa." << endl;
			cout << "\n\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			break;
	}

	return 0;


}
