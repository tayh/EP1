#include "media.hpp"
#include <cstdio>
#include <cstdlib>
#include <vector>


using namespace std;

Media::Media() {
    setLargura(0);
    setAltura(0);
}
Media::~Media() {

}
Media::Media(int largura, int altura) {
    setLargura(largura);
    setAltura(altura);
}

void Media::guardaPixel(ofstream &arquivo2){
    
    int largura, altura;
    int auxR, auxG, auxB;
    int x, y;


    largura = getLargura();
    altura = getAltura();

 
  for(int i = 1; i < altura-1; ++i){
      for(int j = 1; j < largura-1; ++i){

          auxR = 0;
          auxG = 0;
          auxB = 0;

          for(x = i-1; x <= i+1; x++){
            for(y= j-1; y<= j+1; y++){
              auxR += pixelR[x][y];
              auxG += pixelG[x][y];
              auxB += pixelB[x][y];
            }
          }

          pixelR[i][j] = auxR/9;
          pixelG[i][j] = auxG/9;
          pixelB[i][j] = auxB/9;

          arquivo2 << pixelR[i][j];
          arquivo2 << pixelG[i][j];
          arquivo2 << pixelB[i][j];

      }
  }	

}