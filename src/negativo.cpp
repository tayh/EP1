#include "negativo.hpp"
#include <cstdio>
#include <cstdlib>


using namespace std;

Negativo::Negativo() {
    setLargura(0);
    setAltura(0);
    setValorMaximo(0);  
}
Negativo::~Negativo() {

}
Negativo::Negativo(int largura, int altura, int valorMaximo) {
    setLargura(largura);
    setAltura(altura);
    setValorMaximo(valorMaximo);
}

void Negativo::guardaPixel(ofstream &arquivo2){

    
    int largura, altura, valorMaximo;

    largura = getLargura();
    altura = getAltura();
    valorMaximo = getValorMaximo();


    for(int i = 0; i < altura; i++){
        for(int j = 0; j < largura; j++){
            pixelR[i][j] = valorMaximo - pixelR[i][j];
            arquivo2 << pixelR[i][j];
            pixelG[i][j] = valorMaximo - pixelG[i][j];
            arquivo2 << pixelG[i][j];
            pixelB[i][j] = valorMaximo - pixelB[i][j];
            arquivo2 << pixelB[i][j];
        }
    }
		

}