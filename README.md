# EP1 - OO (UnB - Gama)

Orientação a Objetos 2/2016

Taynara de Jesus Carvalho 15/0149301

Aplicando filtros em imagens de formato PPM.

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.


O objetivo do programa é aplicar 4 filtros (negativo, polarizado, preto e branco e de media) em uma imagem no formato PPM.

A linguagem utilizada foi C++.

O código segue o paradigma de orientação a objetos. Nele é usado os conceitos de Herança e Polimorfismo.

# Como compilar e executar

* Para executar este programa, primeiro abra o terminal do seu computador com o sistema operacional Linux;
* Encontre o diretório raiz do programa e abra-o;
* Para limpar os arquivos objetos digite "make clean";
* Para compilar digite "make";
* Para rodar o programa digite "make run"; 
