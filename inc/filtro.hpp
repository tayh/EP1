#ifndef FILTRO_HPP
#define FILTRO_HPP 

#include "imagem.hpp"

using namespace std;

class Filtro : public Imagem {
private:
	int div;
	int size;

public:
	Filtro();
	Filtro(int div, int size);

	int getDiv();
	void setDiv(int div);

	int getSize();
	void setSize(int size);

};

#endif