#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP 

#include <iostream>
#include "filtro.hpp"

class Negativo : public Filtro {
public:

	Negativo();
	~Negativo();
	Negativo(int largura, int altura, int valorMaximo);

	void guardaPixel(ofstream &arquivo2);
};

#endif