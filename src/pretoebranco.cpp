#include "pretoebranco.hpp"
#include <cstdio>
#include <cstdlib>


using namespace std;

Pretoebranco::Pretoebranco() {
    setLargura(0);
    setAltura(0);
}
Pretoebranco::~Pretoebranco() {

}
Pretoebranco::Pretoebranco(int largura, int altura) {
    setLargura(largura);
    setAltura(altura);
}

void Pretoebranco::guardaPixel(ofstream &arquivo2){
    
    int largura, altura;
    char grayscale_value;

    largura = getLargura();
    altura = getAltura();

for(int i = 0; i < altura; ++i){
    for(int j = 0; j < largura; ++j){
           grayscale_value = (0.299 * pixelR[i][j]) + (0.587 * pixelG[i][j]) + (0.144 * pixelB[i][j]);

           pixelR[i][j] = grayscale_value;
           arquivo2 << pixelR[i][j];
           pixelG[i][j] = grayscale_value;
           arquivo2 << pixelG[i][j];
           pixelB[i][j] = grayscale_value;
           arquivo2 << pixelB[i][j];
 
    }
}	

}