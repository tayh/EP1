#include "polarizado.hpp"
#include <cstdio>
#include <cstdlib>


using namespace std;

Polarizado::Polarizado() {
    setLargura(0);
    setAltura(0);
    setValorMaximo(0);  
}
Polarizado::~Polarizado() {

}
Polarizado::Polarizado(int largura, int altura, int valorMaximo) {
    setLargura(largura);
    setAltura(altura);
    setValorMaximo(valorMaximo);
}

void Polarizado::guardaPixel(ofstream &arquivo2){
    
    int largura, altura, valorMaximo;

    largura = getLargura();
    altura = getAltura();
    valorMaximo = getValorMaximo();

    int auxR, auxG, auxB;


for(int i = 0; i < altura; i++){
    for(int j = 0; j < largura; j++){
            auxR = pixelR[i][j];
           if(auxR > valorMaximo/2){
               pixelR[i][j] = valorMaximo;
           }else{
               pixelR[i][j] = 0;
           }
           auxG = pixelG[i][j];
           if(auxG > valorMaximo/2){
               pixelG[i][j] = valorMaximo;
           }else{
               pixelG[i][j] = 0;
           }
           auxB = pixelB[i][j];
           if(auxB > valorMaximo/2){
               pixelB[i][j] = valorMaximo;
           }else{
               pixelB[i][j] = 0;
           }

           arquivo2 << (char)pixelR[i][j];
           arquivo2 << (char)pixelG[i][j];
           arquivo2 << (char)pixelB[i][j];
    }
}
		

}