#include "imagem.hpp"
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <sstream>


Imagem::Imagem(){
	setNumeroMagico("P6");
	setComentario("");
	setLargura(0);
	setAltura(0);
	setValorMaximo(255);
	setNomeDoArquivo2("");


	pixelR = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelR[i] = new unsigned char[largura];
	
	pixelG = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelG[i] = new unsigned char[largura];

	pixelB = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelB[i] = new unsigned char[largura];

}

Imagem::Imagem(string numeroMagico, string comentario, int altura, int largura, int valorMaximo, string nomedoarquivo2) {
	setNumeroMagico(numeroMagico);
	setComentario(comentario);
	setLargura(largura);
	setAltura(altura);
	setValorMaximo(valorMaximo);
	setNomeDoArquivo2(nomedoarquivo2);
}

string Imagem::getNumeroMagico() {
	return numeroMagico;
}
void Imagem::setNumeroMagico(string numeroMagico) {
	this->numeroMagico = numeroMagico;
}

string Imagem::getComentario() {
	return comentario;
}
void Imagem::setComentario(string comentario) {
	this->comentario = comentario;
}

int Imagem::getLargura() {
	return largura;
}
void Imagem::setLargura(int largura) {
	this->largura = largura;
}

int Imagem::getAltura() {
	return altura;
}
void Imagem::setAltura(int altura) {
	this-> altura = altura;
}

int Imagem::getValorMaximo(){
	return valorMaximo;
}
void Imagem::setValorMaximo(int valorMaximo){
	this->valorMaximo = valorMaximo;
}

string Imagem::getNomeDoArquivo2(){
	return nomedoarquivo2;
}
void Imagem::setNomeDoArquivo2(string nomedoarquivo2){
	this->nomedoarquivo2 = nomedoarquivo2;
}


void Imagem::lerImagem(ofstream &arquivo2) {

	ifstream arquivo;
	string nome_do_arquivo;
	cout << "Digite o nome da imagem a ser filtrada: ";
	cin >> nome_do_arquivo;
	nome_do_arquivo = "./doc/" + nome_do_arquivo + ".ppm";
	arquivo.open(nome_do_arquivo.c_str(), ios::binary);
	if(!arquivo.is_open()){
		cout << "Arquivo não encontrado! Saindo..." << endl;
	}

	string numeroMagico, comentario;
	int largura, altura, valorMaximo;
	getline(arquivo, numeroMagico);
	
	if(numeroMagico != "P6"){
		cout << "Tipo de imagem inválida! Saindo..." << endl;
		return;
	}

	string nomedoarquivo2;
	cout << "Digite o nome da nova imagem que foi alterada pelo filtro selecionado: " << endl;
	cin >> nomedoarquivo2;
	nomedoarquivo2 = "./doc/" + nomedoarquivo2 + ".ppm";
	while(nomedoarquivo2 == nome_do_arquivo){
		cout << "Nome de arquivo inválido. Por favor, insira um novo nome: ";
		cin >> nomedoarquivo2;
	}

	arquivo2.open(nomedoarquivo2.c_str(),ios::binary);

	while(1){
		getline(arquivo, comentario);
		if(comentario[0] != '#'){
			int tamanho = comentario.length()+1;
			arquivo.seekg(-tamanho,ios_base::cur);
			arquivo >> largura;
			arquivo >> altura;
			arquivo >> valorMaximo;
	break;
		
		}
	}

	setNumeroMagico(numeroMagico);
	setLargura(largura);
	setAltura(altura);
	setValorMaximo(valorMaximo);

	arquivo2 << numeroMagico << endl;
  	arquivo2 << largura << " " << altura << endl;
  	arquivo2 << valorMaximo << endl;


	arquivo.seekg(-2, ios_base::cur);

	pixelR = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelR[i] = new unsigned char[largura];
	
	pixelG = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelG[i] = new unsigned char[largura];

	pixelB = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	pixelB[i] = new unsigned char[largura];

	char r, g, b;
	for(int i = 0; i < altura; i++){
   		 for(int j = 0; j < largura; j++){
      			arquivo.get(r);
      			arquivo.get(g);
      			arquivo.get(b);
      			pixelR[i][j] = r;
      			pixelG[i][j] = g;
      			pixelB[i][j] = b;
      			
         	}
  	}

  	guardaPixel(arquivo2);

	arquivo.close();
	arquivo2.close();
}

void Imagem::guardaPixel(ofstream &arquivo2) {
	int **auxR, **auxG, **auxB;


	auxR = new int*[altura];
	for(int i = 0; i < altura; i++)
	auxR[i] = new int[largura];

	auxG = new int*[altura];
	for(int i = 0; i < altura; i++)
	auxG[i] = new int[largura];

	auxB = new int*[altura];
	for(int i = 0; i < altura; i++)
	auxB[i] = new int[largura];


		int i, j;

for(i = 0; i < altura; i++){
    for(j = 0; j < largura; j++) {
        auxR[i][j] = (unsigned int)pixelR[i][j];
        arquivo2 << (char)auxR[i][j];
        auxG[i][j] = (unsigned int)pixelG[i][j];
        arquivo2 << (char)auxG[i][j];
        auxB[i][j] = (unsigned int)pixelB[i][j];
        arquivo2 << (char)auxB[i][j];
    }
}

}


	















