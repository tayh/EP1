#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


using namespace std;


class Imagem {
private:

	string numeroMagico, comentario, nomedoarquivo2;
	int largura, altura, valorMaximo;

public:
	
	ofstream arquivo2;
	unsigned char **pixelR, **pixelG, **pixelB;
	
	Imagem();
	Imagem(string numeroMagico, string comentario, int altura, int largura, int valorMaximo, string nomedoarquivo2);

	string getNumeroMagico();
	void setNumeroMagico(string numeroMagico);

	string getComentario();
	void setComentario(string comentario);

	int getLargura();
	void setLargura(int largura);

	int getAltura();
	void setAltura(int altura);

	int getValorMaximo();
	void setValorMaximo(int valorMaximo);

	string getNomeDoArquivo2();
	void setNomeDoArquivo2(string nomedoarquivo2);

	void lerImagem(ofstream &arquivo2);
	virtual void guardaPixel(ofstream &arquivo2);



		
};

#endif
